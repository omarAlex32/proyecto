<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('inicio');
});

Route::get('folders', function () {
    return view('inicio');
});
// Route::get('ejemplo', function () {
//     return view('ejemplo');
// });

Auth::routes();

// Route::get('/home', 'HomeController@index');

Route::get('/', 'FoldersController@index');

Route::get('/folders', 'FoldersController@index');

Route::post('/folders', 'FoldersController@store');

Route::get('/folders/{folders}', 'FoldersController@show');

Route::patch('/folders', 'FoldersController@update');

Route::delete('/folders', 'FoldersController@delete');

Route::patch('/archivos', 'ArchivosController@update');

Route::delete('/archivos', 'ArchivosController@delete');

Route::post('/archivos/upload', 'ArchivosController@store');

Route::post('/archivos/prueba', 'ArchivosController@prueba');

Route::post('/mover', 'ArchivosController@moverfile');

Route::get('storage/{file}/{nombre?}', 'ArchivosController@download');