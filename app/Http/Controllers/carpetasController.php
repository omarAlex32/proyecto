<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\carpetas as carpetas;

class carpetasController extends Controller
{
    public function store(Request $request){
    	$carpetas= new carpetas;
    	$carpetas->nombre = $request->nombre;
    	$carpetas->padre = $request ->padre;
    	$carpetas->save();
    	return redirect('carpetas');
    }
    public function create(){
    	return \View::make('new');
    }
}
