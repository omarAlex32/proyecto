<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Folder;
use App\Archivo;


class FoldersController extends Controller
{
        public function store()
    {
    	$this->validate(request(),[
			'nombre'=> 'required|max:100',
			'padre'=> 'required|min:1',
            'idpadre'=> 'required|min:1',
            'path'=> 'required|min:1',
            'idpath'=> 'required|min:1'

			]);

		Folder::create(request(['nombre' , 'padre', 'path', 'idpath' , 'idpadre']));
 
        return back()->withInput();
    }

        public function update()
        {
        $this -> validate(request(),[
            'nombre'=> 'required||max:100'
            ]);
         $update = Folder::where("id", request('id'))->update([
            'nombre' => request('nombre'),
             ]); 
         return back()->withInput();
        }

        public function delete()
        {
            $childrenkiller= request('id');
            $deletechild = Folder::where('idpath', 'LIKE', '%/'.$childrenkiller.'/%')->delete(); 
            $delete = Folder::where('id', request('id') )->delete(); 

            $archivos = Archivo::where('idpath', 'LIKE', '%/'.$childrenkiller.'/%')->get();
            $deletearchivos = Archivo::where('idpath', 'LIKE', '%/'.$childrenkiller.'/%')->delete();
            foreach($archivos as $archivo){
                $deletefile = $archivo->nombre_real;
                 \Storage::delete($deletefile);
            }

            return back()->withInput();
        }

    	public function index()
    	{
    		$folders = Folder::where('idpadre', '0')->get();
            $archivos = Archivo::where('idpadre', '0')->get();
    		return view('inicio' , compact('folders', 'archivos'));
    	}



    	public function show(Folder $folders)

	{       $idpath = explode("/", $folders->idpath);
            $path = explode("/",$folders->path);
            $foldersdos = Folder::where('idpadre', $folders->id)->get();
            $archivos = Archivo::where('idpadre', $folders->id)->get();
			return view ('show', compact('foldersdos' , 'folders', 'path', 'idpath','archivos'));
 
	}

}
