<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Archivo;
use DB;

class ArchivosController extends Controller
{
    public function store(Request $request)
    {
        
    	$file=$request->file('file');
    	$nombre_ant = $file->getClientOriginalName();
    	$divide = explode('.', $nombre_ant);
    	$getext =array_pop($divide);

    	$extencion=$getext;
        switch($extencion){
            case 'gz':
            $extencion= 'tar.gz';
            break;
            case 'bz2':
            $extencion= 'tar.bz2';
            break;
            case 'Z':
            $extencion= 'tar.Z';
            break;
            case 'lz':
            $extencion= 'tar.lz';
            break;
            case 'lzma':
            $extencion= 'tar.lzma';
            break;
            case 'xz':
            $extencion= 'tar.xz';
            break;
            default:

            break;
        }
    
    	do{
    	$randomName= substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0 , 20).'.'.$extencion;
    	$archivo = Archivo::where('nombre_real', $randomName)->get();
    	}while(count($archivo)>0);

    	\Storage::disk('local')->put($randomName, \File::get($file));
    	DB::table('archivos')->insert(
    		['nombre_real'=> $randomName,'nombre_ant'=> $nombre_ant,'idpadre'=> request('idpadre') ,'idpath'=> request('idpath') ]
    		);
 
        return back()->withInput();
    }


    public function download($file, $nombre ){
    	$path = public_path().'/files/'.$file;
        $divide = explode('.', $file);
        $getext =array_pop($divide);

        $extencion=$getext;
        switch($extencion){
            case 'gz':
            $extencion= 'tar.gz';
            break;
            case 'bz2':
            $extencion= 'tar.bz2';
            break;
            case 'Z':
            $extencion= 'tar.Z';
            break;
            case 'lz':
            $extencion= 'tar.lz';
            break;
            case 'lzma':
            $extencion= 'tar.lzma';
            break;
            case 'xz':
            $extencion= 'tar.xz';
            break;
            default:

            break;
        }
        $nombre_predown=explode('.', $nombre);
        $nombre_download=$nombre_predown[0].'.'.$extencion;

    	return response()->download($path, $nombre_download); 
    }
    public function update()
        {
        $this -> validate(request(),[
            'nombre'=> 'required||max:100'
            ]);
         $update = Archivo::where("id", request('id'))->update([
            'nombre_ant' => request('nombre'),
             ]); 
         return back()->withInput();
        }

        public function delete()
        {
            $nombre_real=request('nombre_real');
            $childrenkiller= request('id');
            \Storage::delete($nombre_real);
            $delete = Archivo::where('id', request('id') )->delete(); 
            return back()->withInput();
        }

        public function moverfile(Request $request)
        {
            $idfile = request('idfile');
            $idfolder = request('idfolder');
            $info = Archivo::where('id', $idfile)->get();
            foreach($info as $inf){
                $idpath = $inf->idpath;
                $idpadre = $inf->idpadre;
            }
            if($idpadre!='0'){
            $split = explode('/', $idpath);
            $num = count($split);
            $num2 = $num -  1 ;
            $num3 = $num - 2;
            $num4 = $num - 3;
            if($split[$num4]==$idfolder){
                    unset($split[$num2]);
                    unset($split[$num3]);
                    $unsplit = implode('/', $split).'/';
                    $newpath = $unsplit;
                 }else{
                    $newpath = $idpath.$idfolder.'/';
                 }
            }else{
                $newpath = '0/'.$idfolder.'/';
            }

            $update = Archivo::where("id", $idfile)->update([
            'idpadre' => $idfolder, 'idpath' => $newpath
             ]); 
        
         return back()->withInput();

            
        }
}
