@extends('layouts.master')
<title>{{$folders->nombre}}</title>
@section('path')

<?php $i=0; ?>
@foreach($path as $pathcut)
	<?php 
		if($pathcut!=""){
		if($i==0){
 	?>
 	<a href="/folders">{{$pathcut}}</a> &nbsp; / &nbsp;
 	<?php }else{ ?>
	<a href="{{$idpath[$i]}}">{{$pathcut}}</a> &nbsp; / &nbsp; 
	
	<?php 
	} $i++;	}
 	?>
@endforeach

{{$folders->nombre}}
@endsection

@section('nav')
<div id="nav">
			<button class="btn" id="nueva_carpeta">Nuevo folder
			</button>
			<button class="btn" id="subir_archivo">Subir archivo
			</button>
</div>

<div class="oculto nueva-carpeta" >
	<div class="nueva-carpeta-title"> 
		<h3>Nuevo folder</h3>
	</div>
	<div class="nueva-carpeta-cont">
		<form method="POST" action="/folders">
			{{csrf_field()}}
			
			<input type="text" name="nombre" id="name" placeholder="Nombre de la carpeta"/><br>
			<input type="hidden" name="padre" id="padre" value="{{$folders->nombre}}"/>
			<input type="hidden" name="idpadre" id="idpadre" value="{{$folders->id}}"/>
			<input type="hidden" name="path" id="path" value="{{$folders->path}}{{$folders->nombre}}/"/>
			<input type="hidden" name="idpath" id="idpath" value="{{$folders->idpath}}{{$folders->id}}/"/>
			<button class="btn margin10" type="submit" name="enviar_nombre" id="enviar_nombre" />Aceptar</button>
			<button class="btn margin10" id="cancelar_nombre" type="button" name="cancelar_nombre">Cancelar</button>
		</form>
	</div>
</div>

<div class="oculto subir-archivo" >
	<div class="subir-archivo-title">
		<h3>Subir archivo</h3>
	</div>
	<div class="subir-archivo-cont">
		<form method="POST" action="/archivos/upload" enctype="multipart/form-data">
			{{csrf_field()}}
			
			<p>Selecciona el archivo</p>
			<input type="hidden" name="idpadre" id="idpadre" value="{{$folders->id}}"/>
			<input type="hidden" name="idpath" id="idpath" value="{{$folders->idpath}}{{$folders->id}}/"/>
			<input type="file" name="file" required/><br>
			<button class="btn margin10" type="submit" name="subir_file" id="subir_file" />Subir</button>
			<button class="btn margin10" id="cancelar_subir" type="button" name="cancelar_subir">Cancelar</button>
	</form>
	</div>
</div>
@endsection

<div id="container"> 
<!-- @section('padrearea') -->
	
<!-- @endsection -->

@section('workarea')

<div class="contenido">
	<center>
<div style="display:inline-block;" class="makeMeDroppable" nombre="{{$folders->padre}}" folderid="{{$folders->idpadre}}">
<div class="folder_content_padre">
<!-- <a class="folder_link  " href = "/folders/{{$folders->idpadre}}"> -->
	<i class="fa fa-folder-open-o"></i>	
	{{$folders->padre}}
<!-- </a> -->
</div>
</div>
</center>
	<h3 id="subtitle">Carpetas</h3>

@foreach($foldersdos as $folder) 
<div style="display:inline-block;" class="makeMeDroppable" nombre="{{$folder->nombre}}" folderid="{{$folder->id}}">
	<i class="fa fa-trash-o erase" aria-hidden="true" atrib="{{$folder->nombre}}" atribdos="{{$folder->id}}" ></i>
<i class="fa fa-pencil edit" atrib="{{$folder->nombre}}" atribdos="{{$folder->id}}" aria-hidden="true"></i>
<div class="folder_content btn">
<a class="folder_link  " href = "/folders/{{$folder->id}}">
	<i class="fa fa-folder-open-o"></i>	
	{{$folder->nombre}}
</a>
</div>
</div>


@endforeach




<h3 id="subtitle">Archivos</h3>
@foreach($archivos as $archivo) 

<div style="display:inline-block;" class="makeMeDraggable" nombre="{{$archivo->nombre_ant}}" fileid="{{$archivo->id}}">
	<i class="fa fa-trash-o erasefile" aria-hidden="true" atrib="{{$archivo->nombre_ant}}" atribdos="{{$archivo->id}}" atribtres="{{$archivo->nombre_real}}" ></i>
<i class="fa fa-pencil editfile" atrib="{{$archivo->nombre_ant}}" atribdos="{{$archivo->id}}"  aria-hidden="true"></i>
<div class="file btn">
<a class="folder_link" href = "/storage/{{$archivo->nombre_real}}/{{$archivo->nombre_ant}}">
	<i class="fa fa-folder-open-o"></i>	
	{{$archivo->nombre_ant}}
</a>
</div>


</div>


@endforeach
</div>
@endsection
</div>
<form method="POST" action="/archivos/upload"  class="dropzone"  >
			{{csrf_field()}}
			
			<input type="hidden" name="idpadre" id="idpadre" value="{{$folders->id}}"/>
			<input type="hidden" name="idpath" id="idpath" value="{{$folders->idpath}}{{$folders->id}}/"/>
</form>

<form action="/mover" method="post" id="form">
	{{csrf_field()}}
<input type="hidden" name="idfile" id="idfile"></input>
<input type="hidden" name="idfolder" id="idfolder"></input>
</form>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js"></script>
<script type="text/javascript">
 
$( init );
 
function init() {
  $('.makeMeDraggable').draggable({
	revert: true,
	containment: 'document'
});
  $('.makeMeDroppable').droppable( {
	hoverClass: 'hovered',
    drop: handleDropEvent
  } );
}
 
function handleDropEvent( event, ui ) {
    var draggable = ui.draggable;

		var file_id=draggable.attr('fileid');
		var folder_id=$(this).attr('folderid');
		draggable.remove();
		$('#idfile').val(file_id);
		$('#idfolder').val(folder_id);
		$('#form').submit();		
	
}
 
</script>	