
<link rel="stylesheet" type="text/css" href="/css/estilos.css">
<link rel="stylesheet" type="text/css" href="/css/font/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="/css/dropzone.css">
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/dropzone.js"></script>
<script type="text/javascript" src="/js/jQuery.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js"></script>



<div id="header"> 
		<div id="titulo">My Drive</div>	
		@yield('nav')
	<div class="directorio">
	 @yield('path')
</div>
</div>

<!-- _______________________folders______________________________ -->
<div class="oculto editar-carpeta" >
	<div class="editar-carpeta-title">
		<h3>Renombrar folder</h3>
	</div>
	<div class="editar-carpeta-cont">
		<form method="POST" action="/folders">
			<input type="hidden" name="_method" value="PATCH">
			{{csrf_field()}}
			<input type="text" name="nombre" id="newname" class="newname" placeholder="Nombre de la carpeta"/><br>
			<input type="hidden" name="id" id="oldid" class="oldid" /><br>
			<button class="btn margin10" type="submit" name="enviar_nombre" id="enviar_nombre" />Aceptar</button>
			<button class="btn margin10" id="cancelar_editar" type="button" name="cancelar_nombre">Cancelar</button>
	</div>
	</form>
</div>

<div class="oculto borrar-carpeta" >
	<div class="borrar-carpeta-title">
		<h3>Borrar folder</h3>
	</div>
	<div class="borrar-carpeta-cont">
		<form method="POST" action="/folders">
			<input type="hidden" name="_method" value="DELETE">
			{{csrf_field()}}
			
			<p>Se borraran los archivos y folders que contenga</p>
			<input type="text" name="nombre" id="borrar_nombre" class="newname" disabled placeholder="Nombre de la carpeta"/><br>
			<input type="hidden" name="id" id="borrar_id" class="oldid" /><br>
			<p>Continuar?</p>
			<button class="btn margin10" type="submit" name="borrarbtn" id="borrarbtn" />Si</button>
			<button class="btn margin10" id="borrar_cancelar" type="button" name="borrar_cancelar">No</button>
	</div>
	</form>
</div>
<!-- _________________________________archivo___________________________ -->
<div class="oculto editar-archivo" >
	<div class="editar-archivo-title">
		<h3>Renombrar archivo</h3>
	</div>
	<div class="editar-archivo-cont">
		<form method="POST" action="/archivos">
			<input type="hidden" name="_method" value="PATCH">
			{{csrf_field()}}
			<input type="text" name="nombre" id="newnamefile" class="newnamefile inputname" placeholder="Nombre del archivo"/><br>
			<input type="hidden" name="id" id="oldidfile" class="oldidfile" /><br>
			<button class="btn margin10" type="submit" name="enviar_nombre_file" id="enviar_nombre" />Aceptar</button>
			<button class="btn margin10" id="cancelar_editarfile" type="button" name="cancelar_nombre_file">Cancelar</button>
	</div>
	</form>
</div>

<div class="oculto borrar-archivo" >
	<div class="borrar-archivo-title">
		<h3>Borrar archivo</h3>
	</div>
	<div class="borrar-archivo-cont">
		<form method="POST" action="/archivos">
			<input type="hidden" name="_method" value="DELETE">
			{{csrf_field()}}
			
			<p>Se borraran los archivos y folders que contenga</p>
			<input type="text" name="nombre" id="borrar_nombrefile" class="newnamefile inputname" disabled placeholder="Nombre de la carpeta"/><br>
			<input type="hidden" name="id" id="borrar_idfile" class="oldidfile" />
			<input type="hidden" name="nombre_real" id="nombre_real" />
			<p>Continuar?</p>
			<button class="btn margin10" type="submit" name="borrarbtnfile" id="borrarbtnfile" />Si</button>
			<button class="btn margin10" id="borrar_cancelarfile" type="button" name="borrar_cancelarfile">No</button>
	</div>
	</form>
</div>



<div class="pantalla oculto"> </div>


