<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchivosTable extends Migration
{
    /**
     * Run t he migrations.
     *
     * @return void
     */
    public function up()
    { 
         Schema::create('archivos', function (Blueprint $table) {
            $table->increments('id')->index() ;
            $table->string('nombre_real');
            $table->string('nombre_ant');
            $table->string('idpadre');
            $table->string('idpath');
            $table->timestamp('updated_at')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('archivos');
    }
}
